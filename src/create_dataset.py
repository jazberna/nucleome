import numpy
import glob
import csv
from PIL import Image
import glob
from numpy import empty, savez_compressed
import os

def create_chunks_container():
    chunks_container=list()
    chunks = numpy.linspace(0,249000000,101)
    for index,position in enumerate(chunks):
        if index < 100:
            chunks_container.append([int(position),int(chunks[index+1]),0])
    return chunks_container

def fill_container(container,info):
    for chunk in container:
        if chunk[0] <= info[0] and  chunk[1] >= info[1]:
            chunk[2]+=info[2]

def normalize_container(container,max):
    for chunk in container:
        chunk[2]/=max


def process_npy_file(npy_file,side_length):
    chr = numpy.load(npy_file)
    image = Image.fromarray(chr,'L')
    image = image.resize((side_length, side_length))
    chr = numpy.asarray(image)
    chr = chr/numpy.max(chr)
    chr = chr.reshape((side_length , side_length, 1))
    return chr

def generate_dataset(side_length=100):
    all_npy_files = glob.glob("/Users/jazberna/HIC_TIMESERIES/*/*/chr1.npy")
    flat_arrays= empty( (len(all_npy_files) ,side_length, side_length, 1))
    times=['GSE92819','GSE92793','GSE92804','GSE92825','GSE92811']
    global_count=0
    for time in times:
        pattern = "/Users/jazberna/HIC_TIMESERIES/*"+time +"*/*/chr1.npy"

        npy_files = glob.glob(pattern)

        npy_files = list(filter(lambda file: 'M' in file ,npy_files))
        for index, file in enumerate(npy_files):
            flat_array = process_npy_file(file,side_length)
            flat_arrays[global_count]=flat_array
            global_count+=1
    return flat_arrays




hic = generate_dataset(side_length=100)
rna_seq = numpy.empty((19, 100,1))

times = ['zero_hours','one_hours','four_hours','eight_hours','twelve_hours']
global_count=0
for index, time in enumerate(times):
    files=glob.glob('/Users/jazberna/HIC_RNA/'+time+'/*.tsv')
    if time == 'one_hours':
        files=files[0:3]
        #print(files)
        #exit(0)
    for file_index, file in enumerate(files):
        if os.path.isfile(file) == False:
            exit(1)
        chunks_container = create_chunks_container()
        for line in csv.reader(open(file,'r'), delimiter='\t'):
            if '#' in line[0] or 'Geneid' in  line[0]:
                continue
            #Geneid  Chr     Start   End     Strand  Length
            chr = None
            start = None
            end= None
            counts=None
            if ';' in line[1]:
                chr = line[1].split(';')[0]
                start = int(line[2].split(';')[0])
                end = int(line[3].split(';')[0])
            else:
                chr = line[1]
                start = int(line[2])
                end = int(line[3])
            counts = int(line[6])
            if chr == 'chr1':
                fill_container(chunks_container,[start,end,counts])
        max_count = max(map(lambda chunk: chunk[2], chunks_container))
        normalize_container(chunks_container,max_count)
        normalized_counts = list(map(lambda chunk: chunk[2], chunks_container))
        normalized_counts = numpy.array(normalized_counts).reshape(100,1)
        rna_seq[global_count]=normalized_counts
        global_count+=1

numpy.savez_compressed('/Users/jazberna/HIC_RNA/hic_rnaseq_chr1_100.zerotoone.npz',hic=hic,rna_seq=rna_seq)