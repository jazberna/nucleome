import keras
from keras import layers
import numpy as np
from keras.utils.vis_utils import plot_model
from keras.initializers import RandomNormal
import matplotlib.pyplot as plt

def generate_autoencoder(side_length,input_size_rnaseq,encoding_size):
    init = RandomNormal(stddev=0.02)

    input_rnaseq = keras.Input(shape=(input_size_rnaseq), name='input_rnaseq')
    encoded_rna_seq = layers.Dense(input_size_rnaseq, activation='relu', name='encoder_rnaseq',  kernel_initializer=init)(input_rnaseq)

    input_img = keras.Input(shape=(side_length,side_length,1), name='input_hic')
    conv_hic = layers.Conv2D(filters=2,kernel_size=(4,4),strides=(1,1), padding='same', activation='relu')(input_img)
    flat_hic = layers.Flatten()(conv_hic)

    concat = layers.Concatenate()([flat_hic, encoded_rna_seq])

    # this is the encoding layer
    encoded_nucleome = layers.Dense( encoding_size  , activation='relu', name='encoded_nucleome', kernel_initializer=init)(concat)

    decoder_rna = layers.Dense( input_size_rnaseq, activation='sigmoid', name='decoder_rna')(encoded_nucleome)
    decoder_rna = layers.Reshape((input_size_rnaseq,1))(decoder_rna)

    nucleome_nodes = side_length * side_length * 1
    decoder = layers.Dense( nucleome_nodes, name='decoder_layer')(encoded_nucleome)
    decoder = layers.LeakyReLU(alpha=0.1)(decoder)
    decoder_shaped_hic = layers.Reshape( (side_length, side_length ,1 ))(decoder)
    decoder_hic = layers.Conv2D(filters=1,kernel_size=(4,4),strides=(1,1), padding='same', activation='sigmoid')(decoder_shaped_hic)

    autoencoder = keras.Model( [input_img,input_rnaseq], [ decoder_hic, decoder_rna] , name='autoencoder')

    encoder =  keras.Model( [input_img,input_rnaseq], encoded_nucleome , name='encoder')

    input_decoder = keras.Input(shape=(encoding_size,), name='input_decoder')

    # rna_decoder
    decb = autoencoder.layers[-3](input_decoder)
    decb = autoencoder.layers[-1](decb)

    # hic decoder
    decc = autoencoder.layers[-6](input_decoder)
    decc = autoencoder.layers[-5](decc)
    decc = autoencoder.layers[-4](decc)
    decc = autoencoder.layers[-2](decc)

    decoder = keras.Model( input_decoder, [ decc,decb ], name='decoder')

    return autoencoder, encoder, decoder


dataset = np.load('/Users/jazberna/HIC_RNA/hic_rnaseq_chr1_100.zerotoone.npz')
side_length=100
input_size_rnaseq = 100
encoding_size = 300
autoencoder, encoder, decoder = generate_autoencoder(side_length,input_size_rnaseq,encoding_size)
autoencoder.summary()
encoder.summary()
decoder.summary()
plot_model(autoencoder, to_file='autoencoder.png', show_shapes=True, show_layer_names=True)
plot_model(encoder, to_file='encoder.png', show_shapes=True, show_layer_names=True)
plot_model(decoder, to_file='decoder.png', show_shapes=True, show_layer_names=True)

autoencoder.compile(optimizer='adam', loss=['mean_squared_error','mean_squared_error'], metrics=['accuracy'])

autoencoder.fit( [dataset['hic'] , dataset['rna_seq'] ], [ dataset['hic'], dataset['rna_seq'] ],
                     epochs=500,
                     batch_size=19,
                     shuffle=True)


autoencoder.save('autoencoder_nucleome.h5')
encoder.save('encoder_nucleome.h5')
decoder.save('decoder_nucleome.h5')


autoencoder = keras.models.load_model('autoencoder_nucleome.h5')
encoder = keras.models.load_model('encoder_nucleome.h5')
decoder = keras.models.load_model('decoder_nucleome.h5')


plt.subplot(2,2,1)
plt.imshow(np.reshape( dataset['hic'][0],(100,100)))

plt.subplot(2,2,2)
plt.bar( list(range(0,100)) ,np.reshape(dataset['rna_seq'][0],(100)))

predicted = autoencoder.predict([dataset['hic'] , dataset['rna_seq'] ])
plt.subplot(2,2,3)
plt.imshow(np.reshape(predicted[0][0],(100,100)))

plt.subplot(2,2,4)
plt.bar( list(range(0,100)) ,np.reshape(predicted[1][0],(100)))

plt.show()