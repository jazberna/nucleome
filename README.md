# Nucleome (HiC and RNA-seq) differential analysis based on autoencoders
**A proof of concept using data from HiC and RNA-seq experiments where a cell line is exposed to Dexamethasone, a drug used to treat COVID-19.**

#### The original data was published here:
https://doi.org/10.1016/j.cels.2018.06.007

D'Ippolito AM, McDowell IC, Barrera A, Hong LK, Leichter SM, Bartelt LC, Vockley CM, Majoros WH, Safi A, Song L, Gersbach CA, Crawford GE, Reddy TE. Pre-established Chromatin Interactions Mediate the Genomic Response to Glucocorticoids. Cell Syst. 2018 Aug 22;7(2):146-160.e7. doi: 10.1016/j.cels.2018.06.007. Epub 2018 Jul 18. PMID: 30031775.

## Concept:
### Keras models
![keras_model](media/keras_model.png)

### Real vs predicted
![keras_model](media/real_predicted.png)

## Google colab notebook training and example:
https://colab.research.google.com/drive/1W1P3_gN_c3X5KA0NMYK961qVO7DVjCip?usp=sharing

## Training dataset:
The [training dataset](dataset/hic_rnaseq_chr1_100.zerotoone.npz) is based on HiC data from http://sysbio.rnet.missouri.edu/3dgenome/GSDB/ and RNA seq from https://www.encodeproject.org/treatment-time-series/ENCSR897XFT/

The [training dataset](dataset/hic_rnaseq_chr1_100.zerotoone.npz) contains two numpy arrays, 'hic' and 'rna_seq'. Both arrays contain 19 arrays. In the case of the 'hic' the arrays are 100x100x1 chr 1 HiC contact matrices. In the case of 'rna_seq' the arrays are 100x1 chr 1 transcript count bins. There are four arrays (technical replicates) for each exposure time to dexamethasone whith the exception of 1 hour exposure for which there are only three replicates. These are the time corresponding indexes for both the 'hic' and 'rna_seq' arrays d arrays:

Indexes 0 to 3: Hi-C on A549 cell line treated with 100 nM dexamethasone for 0 hours.

Indexes 4 to 6: Hi-C on A549 cell line treated with 100 nM dexamethasone for 1 hours.

Indexes 7 to 10: Hi-C on A549 cell line treated with 100 nM dexamethasone for 4 hours.

Indexes 11 to 14: Hi-C on A549 cell line treated with 100 nM dexamethasone for 8 hours.

Indexes 15 to 18: Hi-C on A549 cell line treated with 100 nM dexamethasone for 12 hours.